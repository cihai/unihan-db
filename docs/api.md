(api)=

# API

```{module} unihan_db

```

```{eval-rst}
.. automodule:: unihan_db.bootstrap
    :members:
```

```{eval-rst}
.. automodule:: unihan_db.tables
    :members:
    :undoc-members:
```
